const express = require("express");
const app = express();
const utils = require("./utils");
const auth = require("./apis/auth");
const admin = require("./apis/admin");
const bodyParser = require("body-parser")
const cors = require("cors");
const user = require("./apis/user")
const config = utils.readConfig()
var client, db; 
app.use(cors())  
utils.database_init(config).then((result) => {
    client = result[0];
    db = result[1];
    if (db){
        console.log("Starting Server!")
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(bodyParser.json());  
        app.use(express.static("public"))
        app.use(auth.verifyToken(db))

          
        // app.use(express.urlencoded());
        // app.use(express.json());
        // console.log(db)
        // console.log("hh")
        
        
        app.post("/login",(req, resp) => {
            console.log("here at login index")
            auth.login(req, resp, db);
            //resp.render("helloo login",{"message":""})
        })
        
        app.post("/books",(req, resp)=>{
            console.log("in addBook");
            admin.add_book(req, resp, db);
        })
        
        app.get("/books",(req, resp)=>{
            console.log("in getBooks");
            admin.get_books(req, resp, db);
        })
        
        app.delete("/books",(req, resp)=>{
            console.log("in delete books");
            admin.delete_book(req, resp, db);
        })
        
        app.post("/issueRequest",(req, resp)=>{
            console.log("in issueRequest");
            user.processIssueRequest(req, resp, db);
        })
        
        app.post("/handleIssueRequest",(req, resp)=>{
            console.log("in handleIssueRequest");
            admin.handleIssueRequest(req, resp, db);
        })
        
        app.post("/acceptBook",(req, resp)=>{
            console.log("in acceptBook");
            admin.acceptBook(req, resp, db);
        })
        
        
        app.post("/users",(req, resp)=>{
            console.log("in addUser");
            admin.addUser(req, resp, db);
        })
        
        app.get("/users",(req, resp)=>{
            console.log("in getUser");
            admin.getUsers(req, resp, db);
        })
        
        app.get("/history",(req, resp)=>{
            console.log("in get history");
            user.get_history(req, resp, db);
        })        
        
        app.delete("/users",(req, resp)=>{
            console.log("in delete user");
            admin.delete_user(req, resp, db);
        })        
        app.delete("/history",(req, resp)=>{
            console.log("in delete history");
            user.delete_history(req, resp, db);
        })        

        app.post("/changePassword",(req, resp)=>{
            console.log("in addUser");
            user.change_password(req, resp, db);    
        })

        app.listen(3000,'0.0.0.0',() => {
            console.log("listening on 3000");
        })
    }
})

