const fs = require('fs');
const MongoClient = require('mongodb').MongoClient;

let utils = {}

utils["readConfig"] = () => {

    let rawdata = fs.readFileSync('config.json');
    let config = JSON.parse(rawdata);
    console.log(config);
    return config
}

utils.database_init = (config) => {
    const url = config.mongo_url;
    var db1 = null;
    const client = new MongoClient(url,{useUnifiedTopology: true});
    var mongoConnectionPromise = client.connect();
    return mongoConnectionPromise.then((client) => {
        db1 = client.db("libapp");
        console.log("Database connection successful!");        
        return [client, db1]
    }).catch( (err) => {
        console.log(err)
        return [client, db1]
    })
}

utils.get_admin_credentials = (username, db) => {
    return new Promise( (resolve, reject) => {
            db.collection("admin").find({"username":username}).toArray().then((array) => {
                if (array.length > 0){
                    adminObj = array[0];
                    return resolve([adminObj.username, adminObj.password]);
                }else{
                    return reject("No records found");
                }    
            });
    });

}

utils.get_user_credentials = (username, db) => {
    return new Promise( (resolve, reject) => {
            db.collection("users").find({"username":username}).toArray().then((array) => {
                if (array.length > 0){
                    userObj = array[0];
                    return resolve([userObj.username, userObj.password]);
                }else{
                    return reject("No records found");
                }    
            });
    });
}

utils.get_credentials = (username, role, db) => {
    if (role=="admin"){
        return utils.get_admin_credentials(username, db);
    }else{
        return utils.get_user_credentials(username, db);
    }
}

utils.insert_token = (token, username, role, db) => {
    updateObj = { $set: {
            "username": username,
            "role":role,
            "token":token,
            "issued_timestamp":new Date()
        }
    }
    return db.collection("authTokens").updateOne({"username":username},updateObj,{"upsert":true})
}

utils.insert_obj = (collection, obj, condition, upsert, db) => {
    return db.collection(collection).updateOne(condition,obj,{"upsert":upsert})
}

utils.get_objects_array = (collection, condition, db) => {
        return db.collection(collection).find(condition).toArray();
}

utils.get_user_details_from_token = (token, db) => {
    return new Promise((resolve,reject)=> {
        db.collection("tokens").find({"token":token}).toArray().then((array) => {
            if (array.length > 0){
                resolve(array[0]);
            }else{
                reject(null);
            }
        }).catch(err => {
            reject(null);
        })
    })
}

utils.delete_obj = (collection, condition, db)=>{
    return db.collection(collection).deleteOne(condition)
}

utils.add_to_user_history = (username, message, timestamp, db) =>{
    obj={}
    obj["username"] = username
    obj["timestamp"] = timestamp
    obj["event"] = message
    db.collection("users_history").insertOne(obj,(err, res)=>{
        if (err){
            console.log(err)
        }else{
            console.log("history inserted")
        }
    })
} 

utils.delete_user_history = (username, db) =>{
    return db.collection("users_history").deleteMany({"username":username})  
} 

utils.update_object = (collection, query, updateObj, db)=>{
    var obj = {
        $set: updateObj
    } 
    return db.collection(collection).updateOne(query, obj)
}

module.exports = utils;