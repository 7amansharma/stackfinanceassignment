const utils = require("../utils");
const moment = require("moment")
const config = utils.readConfig()
admin = {}

admin.add_book = async (req, resp, db) => {
    console.log("inside add book")
    console.log(req.decoded)
    if (req.decoded.role === "admin"){
        var book = req.body
        console.log("add book")
        console.log(book)
        book["status"] = "available"
        book["issuedTo"] = "N/A"
        book["issueReqFrom"] = "N/A"
        book["issueReqType"] = "N/A"
        book["issuedTime"] = "N/A"
        book["pastReturnTime"] = "N/A"
        await utils.insert_obj("books", {$set: book}, {"isbn":book.isbn}, true, db).then(()=>{
            resp.json({"message":"success"})    
        }).catch(err => {
            resp.json(err)
        })
    }else{
        resp.json({
            "message":"Permission Denied"
        })
    }
}

admin.getUsers = async (req, resp, db) => {
    console.log(req.query)
    var condition = {};
    if (req.query.condition == "issued"){
        condition = {}
    }
    await utils.get_objects_array("users", condition, db).then(array => {
        console.log(array);
        var temp_array = []
        array.forEach((item)=>{
            item["MembershipExpiresInDays"] =   (item.membership_days - (moment().diff( moment(item.creation_time),"days")) ) 
            temp_array.push(item)
        })
        resp.json({
            "usersList":JSON.stringify(temp_array)
        })
    })    
}


admin.get_books = async (req, resp, db) => {
    console.log(req.query)
    var condition = {};
    if (req.query.condition == "issued"){
        condition = {}
    }
    var request_time = moment(req.body.timestamp) 
    await utils.get_objects_array("books", condition, db).then(async (array) => {
        console.log(array);
        var users =null;
        await utils.get_objects_array("users", {}, db).then(array => {
             console.log(array);
             users = array
        })    
        if (users){
            console.log(users)
            array.map((book)=>{
                console.log(book)
                if (book.status==="issued"){
                    var issued_time = moment(book.issuedTime)
                    // var libOpenTime = moment(moment().format("YYYY-MM-DD") +" " + config.libOpenTime)
                    var libCloseTime = moment(moment().format("YYYY-MM-DD") +" " + config.libCloseTime)
                    var user = users.filter((user)=>{return user.username===book.issuedTo})[0]
                    if (book.issueReqType==1){
                        if ( (request_time > libCloseTime)  || (request_time.diff(issued_time,"hours") >= user.reading_hours) ){
                            book["pastReturnTime"] = "Yes"
                        }else{
                            book["pastReturnTime"] = "No"
                        }
                    }else if (book.issueReqType==7){
                        var past_days = request_time.diff(issued_time,"days")
                        if (past_days >= config.issueDays) {
                            book["pastReturnTime"] = "Yes"
                        }else{
                            book["pastReturnTime"] = "No"
                        }
                    }else{
                        book["pastReturnTime"] = "N/A"
                    }
                }else{book["pastReturnTime"] = "N/A"}
                
                return book
            })
        }
        resp.json({
            "booksList":JSON.stringify(array)
        })
    })
}

admin.delete_book = async (req, resp, db)=>{
    console.log(req.query)
    if (req.decoded.role === "admin" && req.query.isbn){
        var book_isbn = req.query.isbn;
        await utils.delete_obj("books", {"isbn":book_isbn}, db).then((result)=>{
            console.log(result.deletedCount);
            console.log(typeof result);
            resp.json(
                {
                    "success":true,
                    "isbn": book_isbn
                }
            )
        }).catch(err=>{
            resp.json({
                "success":false,
                "err":err
            })
            console.log(err);
        })
    }else{
        resp.json({
            "success":false,
            "err":"Permission Denied or No key"
        })
    }
}

admin.handleIssueRequest = async (req, resp, db)=>{
    if (req.decoded.role==="admin" && req.body.book){
        var request_from = req.body.book.issueReqFrom
        var request_type = req.body.book.issueReqType
        if (req.body.action==="accept"){
            obj = req.body.book
            obj["status"] = "issued"
            obj["issuedTo"] = obj["issueReqFrom"]
            obj["issueReqFrom"] = "N/A"
            obj["issuedTime"] = req.body.timestamp
            obj["pastReturnTime"] = "No"
        }else{
            obj = req.body.book
            obj["status"] = "available"
            obj["issuedTo"] = "N/A"
            obj["issueReqFrom"] = "N/A"
            obj["issueReqType"] = "N/A"
            obj["issuedTime"] = "N/A"
            obj["pastReturnTime"] = "N/A"
        }
        delete obj["_id"]
        await utils.insert_obj("books", {$set: obj}, {"isbn":obj.isbn},true, db).then(()=>{
            resp.json({
                "success" : true,
                "book":obj
            })
            var message = ""
            if (req.body.action==="accept"){
                message = `Issue request for book:${req.body.book.name} with isbn:${req.body.book.isbn} for ${request_type} days got accepted`
            }else{
                message = `Issue request for book:${req.body.book.name} with isbn:${req.body.book.isbn} for ${request_type} days got declined`
            }
            utils.add_to_user_history(request_from, message, req.body.timestamp, db)
        }).catch(err=>{
            console.log(err)
            resp.json({
                "success":false,
                "err":err
            })
        })

    }

}

admin.acceptBook = async (req, resp, db)=>{
    if (req.decoded.role==="admin" && req.body.isbn){
            var request_from = req.body.issuedTo
            obj = req.body
            obj["status"] = "available"
            obj["issuedTo"] = "N/A"
            obj["issueReqFrom"] = "N/A"
            obj["issueReqType"] = "N/A"
            obj["issuedTime"] = "N/A"
            obj["pastReturnTime"] = "N/A"
            delete obj["_id"]
            await utils.insert_obj("books", {$set: obj}, {"isbn":obj.isbn},true, db).then(()=>{
                resp.json({
                    "success" : true,
                    "book":obj
                })
            var message = `Returned book:${req.body.name} with isbn:${req.body.isbn} `
            utils.add_to_user_history(request_from, message, moment().format(config.timestamp_format), db)            
        }).catch(err=>{
            console.log(err)
            resp.json({
                "success":false,
                "err":err
            })
        })
    }
}


admin.addUser = async (req, resp, db)=>{
    console.log(req.body)
    if (req.decoded.role==="admin" && req.body.username && req.body.password){
        console.log("h1")
        obj = req.body
        obj["MembershipExpiresInDays"] = req.body.membership_days
        console.log("h2")
        await utils.insert_obj("users", {$set: obj}, {"username":obj.username},true, db).then(()=>{
            console.log("h3")
            resp.json({
                "success" : true
            })
        }).catch(err=>{
            console.log("h4")
            console.log(err)
            resp.json({
                "success":false,
                "err":err
            })
        })
    }else{
        console.log("hh")
        resp.json({
            "success":false,
            "err":"Incorrect Body"
        })
    }       

}



admin.delete_user = async (req, resp, db)=>{
    console.log(req.query)
    if (req.decoded.role === "admin" && req.query.username){
        var user_username = req.query.username;
        await utils.delete_obj("users", {"username":user_username}, db).then((result)=>{
            console.log(result.deletedCount);
            console.log(typeof result);
            resp.json(
                {
                    "success":true,
                    "username": user_username
                }
            )
        }).catch(err=>{
            resp.json({
                "success":false,
                "err":err
            })
            console.log(err);
        })
    }else{
        resp.json({
            "success":false,
            "err":"Permission Denied or No key"
        })
    }
}

module.exports = admin;

