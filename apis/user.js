const utils = require("../utils")
const config = utils.readConfig()
const moment = require("moment")
user = {}

user.processIssueRequest = async (req, resp, db)=>{
    if (req.decoded.role === "user" && req.body.timestamp){
        obj = req.body.book
        var libOpenTime = moment(moment().format("YYYY-MM-DD") +" " + config.libOpenTime)
        var libCloseTime = moment(moment().format("YYYY-MM-DD") +" " + config.libCloseTime)
        var requestTime = moment(req.body.timestamp)
        var user = null;
        await utils.get_objects_array("users", {username:req.decoded.username}, db).then(array => {
            user = array[0]
        })
        membership_expires_in_days = (user.membership_days - (moment().diff( moment(user.creation_time),"days")) )
        if ( (requestTime > libOpenTime) && (requestTime < libCloseTime) && ( (membership_expires_in_days > 5) || req.body.type==1 ) ){
            obj["status"] = "requested"
            obj["issuedTo"] = "N/A"
            obj["issueReqFrom"] = req.decoded.username
            obj["issueReqType"] = req.body.type
            obj["pastReturnTime"] = "N/A"
            delete obj["_id"]
            await utils.insert_obj("books", {$set: obj}, {"isbn":obj.isbn},true, db).then(()=>{
                resp.json({
                    "success" : true
                })
                var message = `Issue request sent for book:${req.body.book.name} with isbn:${req.body.book.isbn} for ${req.body.book.issueReqType} days`
                utils.add_to_user_history(req.decoded.username, message, req.body.timestamp, db)
            }).catch(err=>{
                console.log(err)
                resp.json({
                    "success":false,
                    "err":err
                })
            })
        }else{
            if ((membership_expires_in_days <= 0) && req.body.type==7){
                resp.json({
                    "success":false,
                    "err":"MembershipExpired"
                })
            }else if ((membership_expires_in_days <= 5) && req.body.type==7){
                resp.json({
                    "success":false,
                    "err":"Cannot issue for seven days, membership expiring in " + membership_expires_in_days.toString() + " days" 
                })
            }
            else{
                resp.json({
                    "success":false,
                    "err":"LibNotOpen for issuing books"
                })
            }
        }

    }
}

user.get_history = async (req, resp, db) => {
    console.log(req.query)
    var condition = {"username":req.decoded.username};
    var request_time = moment(req.body.timestamp) 
    await utils.get_objects_array("users_history", condition, db).then(async (array) => {
        console.log(array);
        array.sort((a,b)=>{return moment(a.timestamp)<moment(b.timestamp)?1:(moment(a.timestamp)==moment(b.timestamp)?0:-1)})
        console.log(array);
        resp.json({
            "success":true,
            "historyList":JSON.stringify(array)
        })
    }).catch((err)=>{
        console.log(err)
        resp.json({
            "success":false,
            "historyList":"[]"
        })
    })
}

user.change_password = async (req, resp, db) => {
    console.log(req.body)
    var old_password =  req.body.old_password
    var new_password =  req.body.new_password
    var query = {
        username: req.decoded.username,
        password: old_password
    }
    var updateObj = {
        "password": new_password
    }
    await utils.update_object("users", query, updateObj, db).then((result) => {
        var nModified = result.result.nModified
        if (nModified > 0){
            resp.json({
                "success":true,
                "result":result.result
            })
        }else if (result.result.n === 0){
            resp.json({
                "success":false,
                "err":"Incorrect Current Password",
                "result":result.result
            })
        }if (result.result.n > 0){
            resp.json({
                "success":true,
                "err":"New password same as old password",
                "result":result.result
            })
        }else{
            resp.json({
                "success":false,
                "err":"Failed",
                "result":result.result
            })
        }
    }).catch((err)=>{
        console.log(err)
        resp.json({
            "success":false,
            "err":err
        })
    })

}


user.delete_history = (req, resp, db)=>{
    utils.delete_user_history(req.decoded.username, db).then((err, resp)=>{
        if (err){
            resp.json({
                "success":false,
                "err":err
            })
        }else{
            resp.json({
                "success":true,
                "message":resp
            })
        }

    })
}

module.exports = user;