let jwt = require('jsonwebtoken');
const utils = require("../utils");
let config = {
    secret:"amansharma1234"
}

function verifyToken(db){
  var db = db;
  return function checkToken (req, res, next,) {
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
    console.log(req.originalUrl)
    console.log(req.headers)
    if (token) {
      if (token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
      }
      console.log(token)
      jwt.verify(token, config.secret,async (err, decoded) => {
        if (err) {
          console.log("token verification failed");
          res.json({
            "success":false,
            "err":err,
            "message":"token verification failed",
            "code":"jwtToken"
          })
          next();
        } else {
          var username=null;
          var password=null;
          try{
            [username, password] = await utils.get_credentials(decoded.username, decoded.role, db);
            //var userDetails = await utils.get_user_details_from_token(token, db);
            console.log("token verification success");
            req.decoded = decoded;
            next();
            
          }catch(e){
            //res.render("login.ejs",{"message":"Incorrect username, password or role"})
            console.log("user not found or deleted");
            res.json({
              "success":false,
              "err":err,
              "code":"jwtToken"
            })
            console.log(e)
          }    
        }
      });
    }else if (req.originalUrl == "/login"){
      next()
    } else {
      res.json({
        success: false,
        message: 'Authentication failed! Please check the request',
        "code":"jwtToken"
      })
    }
  };
  
}



async function login (req, res, db) {
    var username = req.body.username;
    var password = req.body.password;
    var role = req.body.role;
    // For the given username fetch user from DB
    var adminUsername;
    var adminPassword;
    console.log("here1");
    console.log(req.body);
    try{
      [adminUsername, adminPassword] = await utils.get_credentials(username, role, db);
    }catch(e){
      res.json({
        "success":false,
        "err":"No Records Found"
      })
      console.log(e);
      return; 
    }

    console.log(adminUsername+" , "+adminPassword);
    if (username && password) {
      if (username === adminUsername && password === adminPassword) {
        let token = jwt.sign({"username":username,"role":role},
          config.secret,
          { expiresIn: '24h' // expires in 24 hours
          }
        );
        // return the JWT token for the future API calls
        res.json({
          "success":true,
          "token":token
        })
        // var b = token.split(".")[1];
        // console.log("token payload")
        // console.log(jwt.decode(token));
      } else {
        res.json({
          success: false,
          message: 'Incorrect username or password'
        });
      }
    } else {
      res.json({
        success: false,
        message: 'Authentication failed! Please check the request'
      });
    }
  }

module.exports = {
    "login":login,
    "verifyToken":verifyToken
}